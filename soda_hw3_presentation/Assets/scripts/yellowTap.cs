﻿using UnityEngine;
using System.Collections;

public class yellowTap : MonoBehaviour {

	private attackerController Controller;
	private GameObject attacker;
	public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;

	// This allows use to set how powerful the swipe will be

	protected virtual void OnEnable()
	{
		// Hook into the OnSwipe event
		Lean.LeanTouch.OnFingerTap += OnFingerTap;
	}

	protected virtual void OnDisable()
	{
		// Unhook into the OnSwipe event
		Lean.LeanTouch.OnFingerTap -= OnFingerTap;

	}

	public void OnFingerTap(Lean.LeanFinger finger)
	{
		

					Debug.Log("You tapped");
					attacker = gameObject;
					Controller = attacker.GetComponent<attackerController>();
					Controller.hurt ();



	}
}
