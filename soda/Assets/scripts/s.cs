﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class s: MonoBehaviour {


	public GameObject[] enemies;
	private int amount=0;
	public int max;
	private Vector3 spawnPoint;
	public int SPX_MIN;
	public int SPX_MAX;
	public float SPY;
	public int SPZ_MIN;
	public int SPZ_MAX;
	public int spawn_frequency;
	private List<GameObject> clones=new List<GameObject>();
	public GameObject terrain;
	public GameObject can;






	// Update is called once per frame
	void Update () {

		if (amount <= max) {
			InvokeRepeating ("spawnEnemy", spawn_frequency, 2000F);
		}




	}

	void spawnEnemy ()
	{	GameObject clone;
		int lifeNum = Random.Range (2,10);
		attackerController aController;
		do {
			spawnPoint.x = Random.Range (SPX_MIN, SPX_MAX);
			spawnPoint.y = SPY;
			spawnPoint.z = Random.Range (SPZ_MIN, SPZ_MAX);
		} while(Physics.CheckSphere (spawnPoint, 0.01f));
		int index = Random.Range (0, 4);
		clone=(GameObject)Instantiate(enemies[index], spawnPoint, Quaternion.identity);
		clone.transform.parent = terrain.transform;
		//clone.transform.LookAt (can.transform);
		CancelInvoke("spawnEnemy");
		aController = clone.gameObject.GetComponent<attackerController>();
		aController.setLife (lifeNum);
		amount++;
		clones.Add(clone);

	}
	public void removeAttackers(GameObject attacker)
	{
		clones.Remove (attacker);
	}
}
