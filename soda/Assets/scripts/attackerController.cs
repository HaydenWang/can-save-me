﻿using UnityEngine;
using System.Collections;

public class attackerController : MonoBehaviour {
	//Different state
	enum SceneState{running, climbing,climbingToTop, stand};
	private SceneState currentState;

	//Number over the head of the attacker
	private int life;

	//Get control of spawnController
	private GameObject spawnRef;
	private s spawnControllerRef;


	private GameObject can;
	private Transform liferotation;
	private Vector3 canLocation;
	private float running_speed=8.0f;
	private float climbing_speed=1.0f;
	private float landing_speed=1.2f;
	public GameObject top_target;


	public Vector3 offset;

//	public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;

	// This allows use to set how powerful the swipe will be
	//public float ForceMultiplier = 1.0f;

	private Animator mAnimator;
	// Use this for initialization
	void Start () {
		//Get control of spawnController
		spawnRef = GameObject.Find("spawnController");
		spawnControllerRef = spawnRef.GetComponent<s>();

		mAnimator = GetComponent<Animator> ();
		can = GameObject.FindGameObjectWithTag ("can");	
		//Rotate towards the can
		Vector3 relativePos = can.transform.position - transform.position;
		relativePos.y = 0;
		Quaternion rotation = Quaternion.LookRotation(relativePos);
		transform.rotation = rotation;

		//Position of the can

		canLocation = can.transform.position;
		canLocation.y = (float)0.5;

	
		currentState = SceneState.running;
	}
	
	// Update is called once per frame
	void Update () {
		//rotate the text towards the camera
		Vector3 relativePos = -(Camera.main.transform.position - transform.FindChild ("life").position) ;
		Quaternion rotation = Quaternion.LookRotation(relativePos);
		transform.FindChild("life").rotation = rotation;

		//Update the number of lives
		GetComponentInChildren<TextMesh> ().text = "" + life;

		//check if the attacker is dead
		if (life <= 0)
			Destroy (gameObject);

		switch (currentState) {
		case SceneState.running:
			MoveTo (canLocation, running_speed);
			break;
		case SceneState.climbing:
			MoveTo (transform.position + offset, climbing_speed);
			break;
		case SceneState.climbingToTop:
			MoveTo(top_target.transform.position, landing_speed);
			break;
		
		case SceneState.stand:

			break;
		}
		// move the attacker to the can


	}
	public void setLife(int lifeValue)
	{
		life = lifeValue;
	}
	void MoveTo(Vector3 targetPos, float speed)
	{
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, targetPos, step);

	}
	public void arriveAtBottom()
	{
		mAnimator.SetBool ("arriveAtBottom",true);
		currentState = SceneState.climbing;
	}
	public void arriveAtTop()
	{
		mAnimator.SetBool ("arriveAtBottom", false);
		mAnimator.SetBool ("arriveAtTop", true);
		currentState = SceneState.climbingToTop;
	}

	public void standUp()
	{
		mAnimator.SetBool ("arriveAtTop", false);
		mAnimator.SetBool ("standUp", true);
		currentState = SceneState.stand;
	}

//	protected virtual void OnEnable()
//	{
//		// Hook into the OnSwipe event
//		Lean.LeanTouch.OnFingerSwipe += OnFingerSwipe;
//	}
//
//	protected virtual void OnDisable()
//	{
//		// Unhook into the OnSwipe event
//		Lean.LeanTouch.OnFingerSwipe -= OnFingerSwipe;
//	}
//
//	public void OnFingerSwipe(Lean.LeanFinger finger)
//	{
//		// Raycast information
//		var ray = finger.GetStartRay();
//		var hit = default(RaycastHit);
//
//		// Was this finger pressed down on a collider?
//		if (Physics.Raycast(ray, out hit, float.PositiveInfinity, LayerMask) == true)
//		{
//			// Was that collider this one?
//			if (hit.collider.gameObject == gameObject)
//			{
//				Debug.Log ("hit");
//				var swipe = finger.SwipeDelta;
//
//				if (swipe.x < -Mathf.Abs(swipe.y)|| swipe.x > Mathf.Abs(swipe.y))
//				{
//					Debug.Log("You swiped right!");
//					life--;
//				}
//			}
//		}
//}
	public void hurt()
	{
		life--;
	}
}
