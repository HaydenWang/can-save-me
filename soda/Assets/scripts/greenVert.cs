﻿using UnityEngine;
using System.Collections;

public class greenVert : MonoBehaviour {

	private attackerController Controller;
	private GameObject attacker;
	public LayerMask LayerMask = UnityEngine.Physics.DefaultRaycastLayers;

	// This allows use to set how powerful the swipe will be

	protected virtual void OnEnable()
	{
		// Hook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe += OnFingerSwipe;
	}

	protected virtual void OnDisable()
	{
		// Unhook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe -= OnFingerSwipe;

	}

	public void OnFingerSwipe(Lean.LeanFinger finger)
	{
		// Raycast information
		var ray = finger.GetStartRay();
		var hit = default(RaycastHit);

		// Was this finger pressed down on a collider?
		if (Physics.Raycast(ray, out hit, float.PositiveInfinity, LayerMask) == true)
		{
			// Was that collider this one?
			if (hit.collider.gameObject == gameObject)
			{
				var swipe = finger.SwipeDelta;

				if (swipe.y < -Mathf.Abs(swipe.x)|| swipe.y > Mathf.Abs(swipe.x))
				{
					Debug.Log("You swiped");
					attacker = gameObject;
					Controller = attacker.GetComponent<attackerController>();
					Controller.hurt ();
				}
			}
		}
	}
}
